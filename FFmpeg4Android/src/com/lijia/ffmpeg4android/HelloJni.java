package com.lijia.ffmpeg4android;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class HelloJni extends Activity {
    static {    
        System.loadLibrary("ffmpeg");    
        System.loadLibrary("hello-jni");    
    }
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/* Create a TextView and set its content.  
         * the text is retrieved by calling a native  
         * function.
         */  
        TextView  tv = new TextView(this);  
        String str = String.valueOf(stringFromJNI());  
        str = str +"\n"+getVoice();  
        tv.setText(str);  
        setContentView(tv);  
        
//		setContentView(R.layout.hello_jni);
	}

    public native int getVoice();     
    public native String  stringFromJNI();  
    public native String  unimplementedStringFromJNI();
}
