package com.lijia.ffmpeg4android;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

public class AudioPlayer {
	private MediaPlayer mPlayer;
	
	public void play(Context c) {
		stop();
		mPlayer = MediaPlayer.create(c, R.raw.one_small_step);
		mPlayer.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer player) {
				stop();
			}
		});
		mPlayer.start();
	}
	
	public void stop() {
		if(null != mPlayer) {
			mPlayer.release();
			mPlayer = null;
		}
	}
}
