package com.lijia.ffmpeg4android.remoteControl.fragment;

import com.actionbarsherlock.app.SherlockFragment;
import com.lijia.ffmpeg4android.R;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class RemoteControlFragment extends SherlockFragment {
	private TextView mSelectedTextView;
	private TextView mWorkingTextView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_remote_control, container,false);
		mSelectedTextView = (TextView)v.findViewById(R.id.fragment_remote_control_selectedTextView);
		mSelectedTextView.setText("0");
		mWorkingTextView = (TextView)v.findViewById(R.id.fragment_remote_control_workingTextView);
		mWorkingTextView.setText("0");
		
		View.OnClickListener numBtnLister = new OnClickListener() {
			@Override
			public void onClick(View v) {
				TextView tv = (TextView)v;
				String working = mWorkingTextView.getText().toString();
				String text = tv.getText().toString();
				if (working.equals("0")) {
					mWorkingTextView.setText(text);
				} else {
					mWorkingTextView.setText(working + text);
				}
			}
		};
		
		TableLayout tableLayout = (TableLayout)v.findViewById(R.id.fragment_remote_control_tableLayout);
		//Layout last is enter button		
		int number = 1;
		for (int i = 2; i < tableLayout.getChildCount() - 1; i++) {
			TableRow tableRow = (TableRow)tableLayout.getChildAt(i);
			for (int j = 0; j < tableRow.getChildCount(); j++) {
				Button button = (Button)tableRow.getChildAt(j);
				button.setText(number + "");
				button.setOnClickListener(numBtnLister);
				number++;
			}
		}
		
		TableRow bottomRow = (TableRow)tableLayout.getChildAt(tableLayout.getChildCount() - 1);
		
		Button deleteButton = (Button)bottomRow.getChildAt(0);
		deleteButton.setText("Delete");
		deleteButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				mWorkingTextView.setText("0");
			}
		});
		
		Button zeroButton = (Button)bottomRow.getChildAt(1);
		zeroButton.setText("0");
		zeroButton.setOnClickListener(numBtnLister);
		
		Button enterButton = (Button)bottomRow.getChildAt(2);
		enterButton.setText("Enter");
		enterButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				CharSequence working = mWorkingTextView.getText();
				if (working.length() > 0) 
					mSelectedTextView.setText(working);
				mWorkingTextView.setText("0");
			}
		});
		return v;
	}
	
}
