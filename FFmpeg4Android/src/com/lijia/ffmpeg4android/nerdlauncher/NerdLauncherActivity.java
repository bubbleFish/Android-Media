package com.lijia.ffmpeg4android.nerdlauncher;

import com.lijia.ffmpeg4android.SingleFragmentActivity;
import com.lijia.ffmpeg4android.nerdlauncher.fragment.NerdLauncherFragment;

import android.support.v4.app.Fragment;

public class NerdLauncherActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new NerdLauncherFragment();
	}

}
