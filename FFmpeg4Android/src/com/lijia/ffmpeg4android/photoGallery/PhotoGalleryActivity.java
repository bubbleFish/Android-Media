package com.lijia.ffmpeg4android.photoGallery;

import com.lijia.ffmpeg4android.SingleFragmentActivity;
import com.lijia.ffmpeg4android.photoGallery.fragment.PhotoGalleryFragment;

import android.support.v4.app.Fragment;

public class PhotoGalleryActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new PhotoGalleryFragment();
	}

}
