package com.lijia.ffmpeg4android;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends Activity {
	private boolean answerIsTrue;
	private TextView answerTextView;
	private Button showAnswerButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cheat);
		answerIsTrue = this.getIntent().getBooleanExtra(MainActivity.EXTRA_ANSWER_IS_TRUE, false);
		answerTextView = (TextView)this.findViewById(R.id.answerTextView);
		showAnswerButton = (Button)this.findViewById(R.id.showAnswerButton);
		showAnswerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(answerIsTrue){
					answerTextView.setText("正确");
				} else {
					answerTextView.setText("错误");
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cheat, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
