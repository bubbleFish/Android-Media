package com.lijia.ffmpeg4android;

import java.util.List;
import java.util.UUID;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.lijia.ffmpeg4android.fragment.CrimeFragment;
import com.lijia.ffmpeg4android.fragment.CrimeFragment.CrimeFragmentDelegate;
import com.lijia.ffmpeg4android.model.Crime;
import com.lijia.ffmpeg4android.model.CrimeLab;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

public class CrimePagerActivity extends SherlockFragmentActivity 
	implements CrimeFragmentDelegate {
	private ViewPager mViewPager;
	private List<Crime> mCrimes;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.mViewPager = new ViewPager(this);
		this.mViewPager.setId(R.id.viewPager);
		this.setContentView(this.mViewPager);
		
		mCrimes = CrimeLab.get(this).getCrimes();
		FragmentManager fm = this.getSupportFragmentManager();
		mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
			@Override
			public int getCount() {
				return mCrimes.size();
			}
			
			@Override
			public Fragment getItem(int pos) {
				Crime crime = mCrimes.get(pos);
				CrimeFragment cf = CrimeFragment.newInstance(crime.getId());
				return cf;
			}
		});
		
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			public void onPageSelected(int pos) {
				Crime crime = mCrimes.get(pos);
				if (null != crime.getTitle()) {
					setTitle(crime.getTitle());
				}	
			}
			public void onPageScrolled(int pos, float posOffset, int posOffsetPixels) { }
			public void onPageScrollStateChanged(int state) {}
		});
		
		UUID id = (UUID)getIntent().getSerializableExtra(CrimeFragment.EXTRA_CRIME_ID);
		int index = CrimeLab.get(this).getCrimeIndex(id);
		mViewPager.setCurrentItem(index);
	}

	@Override
	public void onCrimeUpdated(Crime crime) {
	}	
}
