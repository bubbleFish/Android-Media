package com.lijia.ffmpeg4android;

import java.util.UUID;

import com.lijia.ffmpeg4android.fragment.CrimeFragment;

import android.support.v4.app.Fragment;

public class CrimeActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		UUID crimeId = (UUID)getIntent().getSerializableExtra(CrimeFragment.EXTRA_CRIME_ID);
		return CrimeFragment.newInstance(crimeId);
	}
	
}
