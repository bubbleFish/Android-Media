package com.lijia.ffmpeg4android;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public abstract class SingleFragmentActivity extends SherlockFragmentActivity {
	protected abstract Fragment createFragment();
	
	protected int getLayoutResId() {
		return R.layout.activity_fragment;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutResId());
		FragmentManager fm = this.getSupportFragmentManager();
		Fragment fg = fm.findFragmentById(R.id.fragmentContainer);
		if(fg == null) {
			fg = createFragment();
			fm.beginTransaction().add(R.id.fragmentContainer, fg).commit();
		}
		
	}	
}
