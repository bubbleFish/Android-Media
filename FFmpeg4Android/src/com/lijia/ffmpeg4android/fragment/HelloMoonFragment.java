package com.lijia.ffmpeg4android.fragment;


import com.actionbarsherlock.app.SherlockFragment;
import com.lijia.ffmpeg4android.AudioPlayer;
import com.lijia.ffmpeg4android.R;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class HelloMoonFragment extends SherlockFragment {
	private AudioPlayer mPlayer = new AudioPlayer();
	
	private Button mPlayBtn;
	private Button mStopBtn;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_hello_moon, parent,false);
		mPlayBtn = (Button)v.findViewById(R.id.hellomoon_playButton);
		mPlayBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mPlayer.play(getActivity());
			}
		});
		mStopBtn = (Button)v.findViewById(R.id.hellomoon_stopButton);
		mStopBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mPlayer.stop();
			}
		});		
		return v;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mPlayer.stop();
	}	
}
