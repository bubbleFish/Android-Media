package com.lijia.ffmpeg4android.fragment;

import java.util.List;

import com.actionbarsherlock.app.SherlockListFragment;
import com.lijia.ffmpeg4android.R;
import com.lijia.ffmpeg4android.model.Crime;
import com.lijia.ffmpeg4android.model.CrimeLab;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

public class CrimeListFragment extends SherlockListFragment {
	public static final String TAG = "CrimeListFragment";
	private static final int REQUEST_CODE_CRIME = 1;
	private List<Crime> mCrimes;
	private CrimeListFragmentDelegate mDelegate;
	
	public interface CrimeListFragmentDelegate {
		void OnCrimeItemSelected(Crime crime);
	}
	
	public class CrimeAdapter extends ArrayAdapter<Crime> {
		public CrimeAdapter(List<Crime> crimes) {
			super(getActivity(), 0,crimes);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(R.layout.crime_list_item, null);
			}
			Crime c = getItem(position);
			TextView titleTextView = (TextView)convertView.findViewById(R.id.crime_title);
			titleTextView.setText(c.getTitle());
			CheckBox solvedCheckBox = (CheckBox)convertView.findViewById(R.id.list_item_crime_solvedCheckBox);
			solvedCheckBox.setChecked(c.isSolved());
			TextView dateTextView = (TextView)convertView.findViewById(R.id.crime_date);
			dateTextView.setText(c.getDate().toString());
			return convertView;
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.getActivity().setTitle(R.string.crime_title);
		this.mCrimes = CrimeLab.get(this.getActivity()).getCrimes();
		
		CrimeAdapter adapter = new CrimeAdapter(this.mCrimes);
		this.setListAdapter(adapter);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View v = super.onCreateView(inflater, container, savedInstanceState);
		ListView listView = (ListView)v.findViewById(android.R.id.list);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			registerForContextMenu(listView);
		} else {
			listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
			listView.setMultiChoiceModeListener(new MultiChoiceModeListener() {
				
				@Override
				public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
					return false;
				}
				
				@Override
				public void onDestroyActionMode(ActionMode mode) {
					
				}
				
				@Override
				public boolean onCreateActionMode(ActionMode mode, Menu menu) {
					MenuInflater inflater = mode.getMenuInflater();
					inflater.inflate(R.menu.crime_list_item_context, menu);
					return true;
				}
				
				@Override
				public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
					switch (item.getItemId()) {
					case R.id.menu_item_delete_crime:
						CrimeAdapter adapter = (CrimeAdapter)getListAdapter();
						CrimeLab crimeLab = CrimeLab.get(getActivity());
						for (int i = adapter.getCount() -1 ; i >= 0; i--) {
							if (getListView().isItemChecked(i)) {
								crimeLab.deleteCrime(adapter.getItem(i));
							}
						}
						mode.finish();
						updateUI();
						return true;
					default:
						return false;
					}
				}
				
				@Override
				public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
					
				}
			});
		}
		return v;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Crime c = (Crime)this.getListAdapter().getItem(position);
		Log.d(TAG, c.getTitle() + " was clicked");
		if (mDelegate != null) {
			mDelegate.OnCrimeItemSelected(c);
		}
//		Intent intent = new Intent(this.getActivity(), CrimePagerActivity.class);
//		intent.putExtra(CrimeFragment.EXTRA_CRIME_ID, c.getId());
//		this.startActivityForResult(intent, REQUEST_CODE_CRIME);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult receive");
		if(requestCode == REQUEST_CODE_CRIME) {
		}
	}
	
	@Override
	public void onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu, com.actionbarsherlock.view.MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_crime_list, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		getActivity().getMenuInflater().inflate(R.menu.crime_list_item_context, menu);
	}
		
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		int position = info.position;
		CrimeAdapter adapter = (CrimeAdapter)getListAdapter();
		Crime crime = adapter.getItem(position);
		
		switch (item.getItemId()) {
		case R.id.menu_item_delete_crime:
			CrimeLab.get(getActivity()).deleteCrime(crime);
			updateUI();
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		updateUI();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mDelegate = (CrimeListFragmentDelegate)activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mDelegate = null;
	}
	
	public void updateUI() {
		((CrimeAdapter)getListAdapter()).notifyDataSetChanged();		
	}
}
