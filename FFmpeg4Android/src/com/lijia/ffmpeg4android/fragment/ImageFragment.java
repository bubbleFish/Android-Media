package com.lijia.ffmpeg4android.fragment;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.lijia.dennishucd.util.PictureUtils;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ImageFragment extends SherlockDialogFragment {
	public static final String EXTRA_IMAGE_PATH = "image_path";
	private ImageView mImageView;
	
	private ImageFragment() {
		
	}
	public static ImageFragment newInstance(String filePath) {
		ImageFragment imageFragment = new ImageFragment();
		Bundle args = new Bundle();
		args.putSerializable(EXTRA_IMAGE_PATH, filePath);
		imageFragment.setArguments(args);
		imageFragment.setStyle(SherlockDialogFragment.STYLE_NO_TITLE, 0);
		return imageFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mImageView = new ImageView(getSherlockActivity());
		String filePath = (String)getArguments().getSerializable(EXTRA_IMAGE_PATH);
		BitmapDrawable image = PictureUtils.getScaledDrawable(getSherlockActivity(), filePath);
		mImageView.setImageDrawable(image);
		return mImageView;
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		PictureUtils.cleanImageView(mImageView);
	}
}
