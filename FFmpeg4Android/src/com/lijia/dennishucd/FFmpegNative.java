package com.lijia.dennishucd;

public class FFmpegNative {
    static{  
    	System.loadLibrary("avcodec");  
        System.loadLibrary("avdevice");  
        System.loadLibrary("avfilter");  
        System.loadLibrary("avformat");  
        System.loadLibrary("avutil");  
        System.loadLibrary("swresample");  
        System.loadLibrary("");  
    }  
    public native int avcodec_find_decoder(int codecID); 
}
