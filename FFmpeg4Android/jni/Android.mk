LOCAL_PATH := $(call my-dir)  
  
include $(CLEAR_VARS)  
PATH_TO_FFMPEG_SOURCE:=$(LOCAL_PATH)/prebuilt/ffmpeg    
LOCAL_C_INCLUDES += $(PATH_TO_FFMPEG_SOURCE)    
LOCAL_LDLIBS := -lffmpeg -llog    
LOCAL_MODULE    := hello-jni  
LOCAL_SRC_FILES := hello-jni.c  
  
include $(BUILD_SHARED_LIBRARY)


#LOCAL_PATH := $(call my-dir)
#declare the prebuilt library
#include $(CLEAR_VARS)
#LOCAL_MODULE := ffmpeg-prebuilt
#LOCAL_SRC_FILES := $(LOCAL_PATH)/prebuilt/libffmpeg.so
#LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
#LOCAL_EXPORT_LDLIBS := $(LOCAL_PATH)/prebuilt/libffmpeg.so
#LOCAL_PRELINK_MODULE := true
#include $(PREBUILT_SHARED_LIBRARY)

#the andzop library
#include $(CLEAR_VARS)
#LOCAL_ALLOW_UNDEFINED_SYMBOLS=false
#LOCAL_MODULE := ffmpeg-test-jni
#LOCAL_SRC_FILES := ffmpeg-test-jni.c
#LOCAL_C_INCLUDES := $(LOCAL_PATH)/include
#LOCAL_SHARED_LIBRARY := ffmpeg-prebuilt
#LOCAL_LDLIBS    := -llog -ljnigraphics -lz -lm $(LOCAL_PATH)/prebuilt/libffmpeg.so
#include $(BUILD_SHARED_LIBRARY)