#include <jni.h>
#include <android/log.h>

JNIEXPORT jint JNICALL Java_com_lijia_dennishucd_FFmpegNative_avcodec_1find_1decoder
  (JNIEnv *, jobject, jint)
  {
	AVCodec*codec = NULL;  

    /*register all formats and codecs */  
    av_register_all();  

    codec= avcodec_find_decoder(codecID);  

    if(codec != NULL)  
    {  
		return 0;  
    }  
    else  
    {  
		return -1;  
    }  	
  }
